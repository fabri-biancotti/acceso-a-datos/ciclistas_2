<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ciclista;
use app\models\Equipo;
use app\models\Etapa;
use app\models\Lleva;
use app\models\Maillot;
use app\models\Puerto;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    
     
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
/**/
     public function actionConsulta1a(){

        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) as TOTAL FROM ciclista')
                ->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) as TOTAL FROM ciclista',
           /* 'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]*/
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['TOTAL'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Número de ciclistas que hay.",
            "sql"=>"SELECT COUNT(*) as TOTAL FROM ciclista",
        ]);
    }
    /**/
     public function actionConsulta1(){
        //mediante active record
        $dataProvider= new ActiveDataProvider([
            'query'=>Ciclista::find()->select("COUNT(*) as total"),
            'pagination'=>[
                'pageSize'=>0,
            ]
        ]);
        
          return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) as total FROM ciclista",
            ]);
     }
    /**/
    /**/
     public function actionConsulta2a(){

        $numero = Yii::$app->db
                ->createCommand('SELECT COUNT(*) as TOTAL FROM ciclista WHERE nomequipo="Banesto"')
                ->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) as TOTAL FROM ciclista WHERE nomequipo="Banesto"',
            'totalCount'=>$numero,
            /*'pagination'=>[
                'pageSize' => 0,
            ]*/
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['TOTAL'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de ciclistas que hay.",
            "sql"=>"SELECT COUNT(*) as TOTAL FROM ciclista WHERE nomequipo='Banesto'",
        ]);
    }
    /**/
     public function actionConsulta2(){
        //mediante active record
        $dataProvider= new ActiveDataProvider([
            'query'=>Ciclista::find()->select("COUNT(*) as total")->distinct()->where("nomequipo='Banesto'"),
            'pagination'=>[
                'pageSize'=>0,
            ]
        ]);
        
          return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) as total FROM ciclista WHERE nomequipo='Banesto'",
            ]);
     }
    /**/
     
       public function actionConsulta3a(){

        $numero = Yii::$app->db
                ->createCommand('SELECT avg(edad) as TOTAL FROM ciclista')
                ->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT avg(edad) as TOTAL FROM ciclista',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 0,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['TOTAL'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT avg(edad) as TOTAL FROM ciclista",
        ]);
    }
    /**/
     public function actionConsulta3(){
        //mediante active record
        $dataProvider= new ActiveDataProvider([
            'query'=>Ciclista::find()->select("avg(edad) as total"),
            'pagination'=>[
                'pageSize'=>0,
            ]
        ]);
        
          return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT avg(edad) as total FROM ciclista",
            ]);
     }
     /**/
       /**/
     
       public function actionConsulta4a(){

        $numero = Yii::$app->db
                ->createCommand('SELECT avg(edad) as media FROM ciclista WHERE nomequipo="Banesto"')
                ->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT avg(edad) as media FROM ciclista WHERE nomequipo="Banesto"',
            /*'totalCount'=>$numero,
            /*'pagination'=>[
                'pageSize' => 0,
            ]*/
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['media'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>" La edad media de los del equipo Banesto",
            "sql"=>"SELECT avg(edad) as media FROM ciclista WHERE nomequipo='Banesto'",
        ]);
    }
    /**/
     public function actionConsulta4(){
        //mediante active record
        $dataProvider= new ActiveDataProvider([
            'query'=>Ciclista::find()->select("avg(edad) as total")->where(' nomequipo="Banesto"'),
            'pagination'=>[
                'pageSize'=>0,
            ]
        ]);
        
          return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT avg(edad) as total FROM ciclista WHERE nomequipo='Banesto'",
            ]);
     }
     /**/
       /**/
     
       public function actionConsulta5a(){

        $numero = Yii::$app->db
                ->createCommand('SELECT count(nomequipo), AVG(edad) as media FROM ciclista GROUP BY nomequipo')
                ->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo, AVG(edad) as media FROM ciclista GROUP BY nomequipo',
          /* 'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]*/
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo', 'media'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT nomequipo, AVG(edad) as media FROM ciclista GROUP BY nomequipo",
        ]);
    }
    /**/
     public function actionConsulta5(){
        //mediante active record
        $dataProvider= new ActiveDataProvider([
            'query'=>Ciclista::find()->select("nomequipo,avg(edad) as media")->groupBy('nomequipo'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
          return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','media'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT nomequipo, AVG(edad) as media FROM ciclista GROUP BY nomequipo",
            ]);
     }
     /**/
       /**/
     
       public function actionConsulta6a(){

        $numero = Yii::$app->db
                ->createCommand('SELECT count(nomequipo),COUNT(nombre) as nombre FROM ciclista 
GROUP BY nomequipo')
                ->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo,COUNT(nombre) as nombre FROM ciclista 
GROUP BY nomequipo',
           /* 'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]*/
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','nombre'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT nomequipo,COUNT(nombre) as nombre FROM ciclista 
GROUP BY nomequipo",
        ]);
    }
    /**/
     public function actionConsulta6(){
           //mediante active record
        $dataProvider= new ActiveDataProvider([
            'query'=>Ciclista::find()->select("nomequipo, count(nombre) as total")->groupBy('nomequipo'),
            'pagination'=>[
                'pageSize'=>0,
            ]
        ]);
        
          return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo','total'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>"SELECT nomequipo,COUNT(nombre) as total FROM ciclista group by nomequipo",
            ]);
     }
     /**/
       /**/
     
       public function actionConsulta7a(){

        $numero = Yii::$app->db
                ->createCommand('select count(*) as total from puerto')
                ->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql'=>'select count(*) as total from puerto',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 0,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"El número total de puertos",
            "sql"=>"select count(*) as total from puerto",
        ]);
    }
    /**/
     public function actionConsulta7(){
        //mediante active record
        $dataProvider= new ActiveDataProvider([
            'query'=> Puerto::find()->select("count(*) as total"),
            'pagination'=>[
                'pageSize'=>0,
            ]
        ]);
        
          return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(*) as total FROM puerto",
            ]);
     }
     /**/
       /**/
     
       public function actionConsulta8a(){

        $numero = Yii::$app->db
                ->createCommand('SELECT count(*) as total  FROM puerto where altura > 1500')
                ->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT count(*) as total FROM puerto where altura > 1500',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 0,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT count(*) as total FROM puerto where altura > 1500 ",
        ]);
    }
    /**/
     public function actionConsulta8(){
        //mediante active record
        $dataProvider= new ActiveDataProvider([
            'query'=>Ciclista::find()->select("COUNT(*) as total"),
            'pagination'=>[
                'pageSize'=>0,
            ]
        ]);
        
          return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT count(*) as total FROM puerto where altura > 1500",
            ]);
     }
     /**/
       /**/
     
       public function actionConsulta9a(){

        $numero = Yii::$app->db
                ->createCommand('SELECT count(nomequipo), COUNT(nombre) AS total FROM ciclista 
GROUP BY nomequipo having count(nombre)>4')
                ->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo, COUNT(nombre) AS total FROM ciclista GROUP BY nomequipo having count(nombre)>4',
            'totalCount'=>$numero,
            'pagination'=>[
                 'pageSize' => 5,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo, COUNT(nombre) AS total FROM ciclista GROUP BY nomequipo having count(nombre)>4",
        ]);
    }
    /**/
     public function actionConsulta9(){
        //mediante active record
        $dataProvider= new ActiveDataProvider([
            'query'=>Ciclista::find()->select("nomequipo, count(nombre) as total")->distinct()->groupBy('nomequipo')->having('count(nombre)>4'),
            'pagination'=>[
                'pageSize'=>0,
            ]
        ]);
        
          return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT count(nomequipo), COUNT(nombre) AS total FROM ciclista 
GROUP BY nomequipo having count(nombre)>4",
            ]);
     }
     /**/
       /**/
     
       public function actionConsulta10a(){

        $numero = Yii::$app->db
                ->createCommand('SELECT count(nomequipo) FROM ciclista 
  WHERE edad BETWEEN 28 AND 32
GROUP BY nomequipo hAVING count(nombre)>4')
                ->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo FROM ciclista 
  WHERE edad BETWEEN 28 AND 32
GROUP BY nomequipo hAVING count(nombre)>4',
            /*'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 0,
            ]*/
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>" Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT nomequipo FROM ciclista 
  WHERE edad BETWEEN 28 AND 32
GROUP BY nomequipo hAVING count(nombre)>4",
        ]);
    }
    /**/
     public function actionConsulta10(){
        //mediante active record
        $dataProvider= new ActiveDataProvider([
            'query'=>Ciclista::find()->select("nomequipo")->distinct()->where('edad BETWEEN 28 AND 32')->groupBy('nomequipo')->having('count(nombre)>4'),
            'pagination'=>[
                'pageSize'=>0,
            ]
        ]);
        
          return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>" Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT nomequipo FROM ciclista 
  WHERE edad BETWEEN 28 AND 32
GROUP BY nomequipo hAVING count(nombre)>4",
            ]);
     }
     /**/
       /**/
     
       public function actionConsulta11a(){

        $numero = Yii::$app->db
                ->createCommand('SELECT count(DISTINCT dorsal), COUNT(numetapa) as total FROM etapa
GROUP BY dorsal;')
                ->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT dorsal, COUNT(numetapa) as total FROM etapa
GROUP BY dorsal',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 0,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','total'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT DISTINCT dorsal, COUNT(numetapa) as total FROM etapa
GROUP BY dorsal;",
        ]);
    }
    /**/
     public function actionConsulta11(){
        //mediante active record
        $dataProvider= new ActiveDataProvider([
            'query'=> Etapa::find()->select("dorsal,COUNT(numetapa) as total")->groupBy('dorsal'),
            'pagination'=>[
                'pageSize'=>0,
            ]
        ]);
        
          return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','total'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT DISTINCT dorsal, COUNT(numetapa) as total FROM etapa
GROUP BY dorsal",
            ]);
     }
     /**/
       /**/
     
       public function actionConsulta12a(){

        $numero = Yii::$app->db
                ->createCommand('SELECT  count(DISTINCT dorsal) FROM etapa
GROUP BY dorsal HAVING(COUNT(numetapa)>1)')
                ->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT dorsal FROM etapa
GROUP BY dorsal HAVING(COUNT(numetapa)>1)',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 0,
            ]
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT DISTINCT dorsal FROM etapa
GROUP BY dorsal HAVING(COUNT(numetapa)>1)",
        ]);
    }
    /**/
     public function actionConsulta12(){
        //mediante active record
        $dataProvider= new ActiveDataProvider([
            'query'=> Etapa::find()->select("dorsal")->distinct()->groupBy('dorsal')->having('count(numetapa)>1'),
            'pagination'=>[
                'pageSize'=>0,
            ]
        ]);
        
          return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT DISTINCT dorsal FROM etapa
GROUP BY dorsal HAVING(COUNT(numetapa)>1",
            ]);
     }
     /**/
    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
