<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Ciclistas 2</h1>
        <h3>Ejercicios de consultas 2 sobre base de datos ciclistas.</h3>

        <!-- <p class="lead">You have successfully created your Yii-powered application.</p> -->

        <!-- <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p> -->
    </div>

    <div class="body-content">

        <div class="row">
             <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 1</h2>
                <p>Número de ciclistas que hay</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta1'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta1a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            <!-- -->
             <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 2</h2>
                <p>Número de ciclistas que hay del equipo Banesto</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta2'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta2a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            <!-- -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 3</h2>
                <p>Edad media de los ciclistas</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta3'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta3a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            <!-- -->
                 <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 4</h2>
                <p>Edad media de los ciclistas</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta4'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta4a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
              <!-- -->
               <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 5</h2>
                <p>Edad media de los ciclistas</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta5'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta5a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
              <!-- -->
               <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 6</h2>
                <p>Edad media de los ciclistas</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta6'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta6a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
              <!-- -->
               <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 7</h2>
                <p>Edad media de los ciclistas</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta7'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta7a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
              <!-- -->
               <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 8</h2>
                <p>Edad media de los ciclistas</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta8'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta8a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
              <!-- -->
               <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 9</h2>
                <p>Edad media de los ciclistas</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta9'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta9a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
              <!-- -->
               <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 10</h2>
                <p>Edad media de los ciclistas</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta10'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta10a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
              <!-- -->
               <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 11</h2>
                <p>Edad media de los ciclistas</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta11'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta11a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
              <!-- -->
               <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 12</h2>
                <p>Edad media de los ciclistas</p>
                <p>
                    <?= Html::a('Active Record', ['site/consulta12'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consulta12a'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
              <!-- -->
        </div>
       
    </div>
</div>
